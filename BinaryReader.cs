using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

class BinRead {
    static void Main() 
    {
        var file = File.ReadAllBytes("\\\\vmware-host\\Shared Folders\\Thesis\\subject1_ego\\h1\\0\\cam4\\ply_downsampled_binary\\000068.ply");

        foreach (var line in read_lines(file)) {
            Console.WriteLine(line);
        }
    }

    static IEnumerable<string> read_lines(byte[] bytes)
    {
        StringBuilder res = new StringBuilder();
        foreach (byte b in bytes)
        {
            if (b == '\n')
            {
                string ret = res.ToString();
                res = new StringBuilder();
                yield return ret;
            }
            else
            {
                res.Append(Convert.ToChar(b));
            }
        }
    }
}
