import open3d as o3d
from os.path import join
import numpy as np
import png
import cv2
from utils import *

from sklearn.neighbors import NearestNeighbors

depth_nbrs = None
rgb_nbrs = None


def depth_to_colormapjet(depth):
    depth_color = depth.copy()
    min_d, max_d = np.min(depth_color), np.max(depth_color)
    depth_color = depth_color * 255. / (max_d - min_d)  # - min_d
    depth_color = np.uint8(depth_color)

    depth_color = cv2.applyColorMap(depth_color, cv2.COLORMAP_JET)
    
    return depth_color


def load_depth_and_cam(dir_depth, poses, timings, timestamp, K_parameters_depth):
    global depth_nbrs

    if not depth_nbrs:
        depth_nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(
            timings[:, 1].reshape(-1, 1))

    _, frame_number_depth = depth_nbrs.kneighbors(
        np.array(float(timestamp) + 0 * (10 ** 4)).reshape(-1, 1))
    frame_number_depth = frame_number_depth[0][0]

    filename_depth = join(dir_depth, '{:06d}.png'.format(frame_number_depth))

    depth = load_depth(filename_depth)

    M_depth = poses[frame_number_depth, 1:].reshape(4, 4).copy()

    cam_depth = {}
    K_depth = K_parameters_depth[:9].reshape(3, 3)
    M_depth[:3, 3] *= 1000
    M_depth = np.dot(axis_transform, np.linalg.inv(M_depth))

    cam_depth['K_dist'] = K_depth
    cam_depth['M_dist'] = M_depth

    return depth, cam_depth



def create_point_cloud_from_depth(depth, cam_depth, remove_outlier=True, remove_close_to_cam=300):
    K_depth = cam_depth['K_dist']

    # depth = cv2.undistort(
    #     depth, K_depth, dist_coeffs, None, K_depth,
    # )

    img2d_converted = depthConversion(depth, K_depth[0][0], K_depth[0][2], K_depth[1][2])
    img2d_converted_color = depth_to_colormapjet(img2d_converted)

    cv2.imshow('img2d_converted_color', img2d_converted_color)

    points = generatepointcloud(img2d_converted, K_depth[0][0], K_depth[1][1], K_depth[0][2], K_depth[1][2])
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)

    if remove_outlier:
        pcd, _ = pcd.remove_radius_outlier(nb_points=10, radius=10)

    if remove_close_to_cam > 0:
        center = np.array([0, 0, 0])
        R = np.eye(3)
        extent = np.array([remove_close_to_cam, remove_close_to_cam, remove_close_to_cam])
        bb = o3d.geometry.OrientedBoundingBox(center, R, extent)
        close_points_indices = bb.get_point_indices_within_bounding_box(pcd.points)
        pcd = pcd.select_by_index(close_points_indices, invert=True) #select outside points
        bb.color = np.array([0.1, 0.1, 0.7])

    return pcd



def load_rgb_and_cam(dir_rgb, poses_rgb, timing_rgb, time_stamp, K_parameters_rgb):
    global rgb_nbrs

    if not rgb_nbrs:
        rgb_nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(
            timing_rgb[:, 1].reshape(-1, 1))

    _, frame_number_rgb = rgb_nbrs.kneighbors(
        np.array(float(time_stamp) + 0 * (10 ** 4)).reshape(-1, 1))
    frame_number_rgb = frame_number_rgb[0][0]

    filename_rgb = join(dir_rgb, '{:06d}.png'.format(frame_number_rgb))

    rgb = cv2.imread(filename_rgb)

    K_color = K_parameters_rgb[:9].reshape(3, 3)
    M_color = poses_rgb[frame_number_rgb, 1:].reshape(4, 4).copy()

    M_color[:3, 3] *= 1000

    M_color = np.dot(axis_transform, np.linalg.inv(M_color))

    cam_rgb = {}
    cam_rgb['K_color'] = K_color
    cam_rgb['M_color'] = M_color

    return rgb, cam_rgb


if __name__ == "__main__":
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    opt = vis.get_render_option()
    opt.point_size = 4

    dir_seq = 'D:\data\Temp\HoloLensCapture.0062\HoloLensCapture.0062\Export'

    dir_depth = join(dir_seq, 'AhatDepth')
    dir_rgb = join(dir_seq, 'Video')

    # Depth
    poses_depth = np.loadtxt(join(dir_depth, 'Pose.txt'))
    timing_depth = np.loadtxt(join(dir_depth, 'Timing.txt'))

    K_parameters_depth = np.loadtxt(join(dir_depth, 'Intrinsics.txt'))
    dist_coeffs = np.array(K_parameters_depth[9:14]).astype('float32')
    w_depth, h_depth = [int(x) for x in K_parameters_depth[-2:]]

    # RGB
    poses_rgb = np.loadtxt(join(dir_rgb, 'Pose.txt'))
    timing_rgb = np.loadtxt(join(dir_rgb, 'Timing.txt'))
    K_parameters_rgb = np.loadtxt(join(dir_rgb, 'Intrinsics.txt'))
    w_color, h_color = [int(x) for x in K_parameters_rgb[-2:]]

    frame_number_depth = 280
    time_stamp = timing_depth[frame_number_depth, 1]

    depth, cam_depth_calib = load_depth_and_cam(dir_depth,
                                                poses_depth,
                                                timing_depth,
                                                time_stamp,
                                                K_parameters_depth)
    
    K_depth = cam_depth_calib['K_dist']
    depth_undistort = cv2.undistort(
        depth, K_depth, dist_coeffs, None, K_depth,
    )
    
    rgb, cam_rgb_calib = load_rgb_and_cam(dir_rgb,
                                          poses_rgb,
                                          timing_rgb,
                                          time_stamp,
                                          K_parameters_rgb)
    
    
    depth_color = depth_to_colormapjet(depth)
    cv2.imshow('rgb', rgb)
    cv2.imshow('depth', depth_color)
    
    pcd = create_point_cloud_from_depth(depth_undistort, cam_depth_calib, remove_outlier=True, remove_close_to_cam=300)
    pcd_colored = get_colored_pcd(pcd, rgb, cam_rgb_calib, cam_depth_calib)

    vis.add_geometry(pcd_colored)
    vis.poll_events()
    vis.update_renderer()

    depth_aligned = map_depth_to_rgb(pcd, rgb, cam_rgb_calib, cam_depth_calib, reference='depth')
    depth_aligned_color = depth_to_colormapjet(depth_aligned)
    # depth_aligned_color = cv2.medianBlur(depth_aligned_color, 2)
    # depth_aligned_color = cv2.bilateralFilter(depth_aligned_color, 9, 75, 75)
    cv2.imshow('depth_aligned', depth_aligned_color)

    cv2.waitKey(10)
    
    vis.run()

    