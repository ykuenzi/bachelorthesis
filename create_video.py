# import numpy as np
import cv2
import os
from pathlib import Path

def main():
    rgbPath = Path("ImageCloud", "rgb_ply")
    rgb_images = list(filter(lambda x: x.endswith(".png"),
                             os.listdir(rgbPath.as_posix())))
    rgb_images.sort()

    h, w = 504, 896
    out = cv2.VideoWriter("video.mp4", cv2.VideoWriter_fourcc(*'mp4v'), 30,
                          (w, h))
    for name in rgb_images:
        print(f"Adding {name}")
        img = cv2.imread(rgbPath.joinpath(name).as_posix())
        out.write(img)

    out.release()


if __name__ == "__main__":
    main()
