import open3d as o3d
from os.path import join
import numpy as np
# import png
import cv2
from utils import *

from sklearn.neighbors import NearestNeighbors
from scipy.spatial.transform import Rotation

depth_nbrs = None
rgb_nbrs = None
eyes_nbrs = None
hand_nbrs = None



def create_bounding_box_3d(center):
    R = np.identity(3)
    extent = np.ones(3) * 3000  # trying to create a bounding box 50 cm
    bb_3d = o3d.geometry.OrientedBoundingBox(center, R, extent)
    return bb_3d


def create_point_cloud_from_depth(dir_depth, frame_number, visualize=True, depth_type='ahat'):
    filename_depth = join(dir_depth, '{:06d}.png'.format(frame_number))

    # filename_depth = './data/yanik/point_cloud_2306/000019_depth.png'


    depth = load_depth(filename_depth)
    depth = depth[::-1, :]

    # K_depth = np.loadtxt(join(dir_depth, 'K_{:06d}.txt'.format(frame_number)))[:3, :3]
    M_depth = np.loadtxt(join(dir_depth, 'M_{:06d}.txt'.format(frame_number)))

    # M_depth = np.loadtxt('./data/yanik/point_cloud/M_depth.txt')

    cam_depth_calib = {}
    M_depth[:3, 3] *= 1000

    new_axis_transform = np.eye(4)
    new_axis_transform[0, 0] = -1
    new_axis_transform[1, 1] = -1

    # M_depth = np.dot(new_axis_transform, np.linalg.inv(M_depth))
    M_depth = np.dot(np.eye(4), np.linalg.inv(M_depth))



    dist_coeffs = np.array([-0.22356547153865305, 0.03186705146154515, 0, 0, 0])

    K_depth = np.array([[245.75893514,   0.,         256.89128253 ],
                        [ 0.,        246.00045934, 255.23097475],
                        [ 0.,           0.,           1.     ]])

    # K_depth = np.array([[2.208e+02,   0.,         2.577e+02 ],
                          #                         [ 0.,        2.211e+02, 2.552e+02],
                          #                         [ 0.,           0.,           1.     ]])

    cam_depth_calib['K_dist'] = K_depth
    cam_depth_calib['M_dist'] = M_depth

    depth = cv2.undistort(
            depth, K_depth, dist_coeffs, None, K_depth,
            )

    if visualize:
        depth_color = depth.copy()
        min_d, max_d = np.min(depth_color), np.max(depth_color)
        depth_color = depth_color * 255. / (max_d - min_d)  # - min_d
        depth_color = np.uint8(depth_color)

        depth_color = cv2.applyColorMap(depth_color, cv2.COLORMAP_JET)
        cv2.imshow('depth', depth_color)
        cv2.imwrite('depth_2.png', depth_color)

    img2d_converted = depthConversion(depth, K_depth[0][0],  K_depth[0][2], K_depth[1][2])
    points = generatepointcloud(img2d_converted, K_depth[0][0], K_depth[1][1], K_depth[0][2], K_depth[1][2])
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)

    if depth_type == 'ahat':
        pcd, ind = pcd.remove_radius_outlier(nb_points=10, radius=10)

    remove_close_to_cam = 300
    if remove_close_to_cam > 0:
        center = np.array([0, 0, 0])
            R = np.eye(3)
            extent = np.array([remove_close_to_cam, remove_close_to_cam, remove_close_to_cam])
            bb = o3d.geometry.OrientedBoundingBox(center, R, extent)
            close_points_indices = bb.get_point_indices_within_bounding_box(pcd.points)
            pcd = pcd.select_by_index(close_points_indices, invert=True) #select outside points

    else:
        bb_3d = create_bounding_box_3d([0, 0, 0])
        inliers_indices = bb_3d.get_point_indices_within_bounding_box(pcd.points)
        pcd = pcd.select_by_index(inliers_indices, invert=True)
        pcd, ind = pcd.remove_radius_outlier(nb_points=10, radius=50)


    h_depth, w_depth = depth.shape[:2]


    cam_depth = o3d.geometry.TriangleMesh.create_sphere(radius=10.0)
    cam_depth.compute_vertex_normals()
    cam_depth.paint_uniform_color([0.1, 0.1, 0.7])
    cam_depth.transform(np.linalg.inv(M_depth))
    cam_depth_origin = o3d.geometry.LineSet.create_camera_visualization(w_depth, h_depth, K_depth, M_depth, scale=500)
    cam_depth_origin.paint_uniform_color([0.1, 0.1, 0.7])

    return depth, cam_depth_calib, pcd, cam_depth, cam_depth_origin


def load_rgb(dir_rgb, frame_number, visualize=True):
    filename_rgb = join(dir_rgb, '{:06d}.png'.format(frame_number))

    # filename_rgb = './data/yanik/point_cloud/000019.png'
    rgb = cv2.imread(filename_rgb)
    # rgb = rgb[::-1, :, :]
    # rgb = rgb[::-1, ::-1, :]

    if visualize:
        cv2.imshow('rgb', rgb)


    K_color = np.loadtxt(join(dir_rgb, 'K_{:06d}.txt'.format(frame_number)))[:3, :3]
    M_color = np.loadtxt(join(dir_rgb, 'M_{:06d}.txt'.format(frame_number)))

    # K_color = np.loadtxt('./data/yanik/point_cloud/K_000019.txt')[:3, :3] 
    # M_color = np.loadtxt('./data/yanik/point_cloud/M_000019.txt')


    M_color[:3, 3] *= 1000
    new_axis_transform = np.eye(4)
    new_axis_transform[2, 2] = -1
    new_axis_transform[0, 0] = -1
    M_color = np.dot(new_axis_transform, np.linalg.inv(M_color))

    new_axis_transform = np.eye(4)
    new_axis_transform[0, 0] = -1
    new_axis_transform[1, 1] = -1

    M_color = np.dot(new_axis_transform, M_color)

    cam_rgb = {}
    cam_rgb['K_color'] = K_color
    cam_rgb['M_color'] = M_color

    h_color, w_color = rgb.shape[:2]

    cam_color = o3d.geometry.TriangleMesh.create_sphere(radius=10.0)
    cam_color.compute_vertex_normals()
    cam_color.paint_uniform_color([0.7, 0.1, 0.1])
    cam_color.transform(np.linalg.inv(M_color))
    cam_color_origin = o3d.geometry.LineSet.create_camera_visualization(w_color, h_color, K_color, M_color, scale=500)
    cam_color_origin.paint_uniform_color([0.7, 0.1, 0.1])

    return rgb, cam_rgb, cam_color, cam_color_origin


def get_eye_gaze(poses_eye, time_stamp):
    global eyes_nbrs

    if not eyes_nbrs:
        timing_eye = poses_eye[:, 0]
        eyes_nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(
                timing_eye.reshape(-1, 1))

    _, frame_number_eye = eyes_nbrs.kneighbors(
            np.array(float(time_stamp) + 0 * (10 ** 4)).reshape(-1, 1))
    frame_number_eye = frame_number_eye[0][0]

    M_eye = poses_eye[frame_number_eye, 1:7].reshape(2, 3) * 1000

    eye_point_origin = o3d.geometry.TriangleMesh.create_sphere(radius=10.0)
    eye_point_origin.compute_vertex_normals()
    eye_point_origin.paint_uniform_color([0.1, 0.7, 0.5])
    eye_transform = np.eye(4)
    eye_transform[:3, 3] = M_eye[0, :]
    eye_point_origin.transform(eye_transform)


    eye_norm = np.linalg.norm(M_eye[1, :])
    M_eye[1, :] = M_eye[0, :] + 1000 * (M_eye[1, :]) / eye_norm

    eye_gaze = o3d.geometry.LineSet()
    eye_gaze.points = o3d.utility.Vector3dVector(M_eye)
    eye_gaze.lines = o3d.utility.Vector2iVector([[0, 1]])
    eye_gaze.paint_uniform_color([0.1, 0.7, 0.1])

    return eye_gaze, eye_point_origin

def get_hand_joints(poses_hand, time_stamp):
    global hand_nbrs

    if not hand_nbrs:
        timing_hand = poses_hand[:, 0]
        hand_nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(
                timing_hand.reshape(-1, 1))

    _, frame_number_hand = hand_nbrs.kneighbors(
            np.array(float(time_stamp) + 0 * (10 ** 4)).reshape(-1, 1))
    frame_number_hand = frame_number_hand[0][0]

    hand_point = poses_hand[frame_number_hand][4:].reshape(
            3, -1)

    joints = []
    joint_transform = np.eye(4)
    for joint_i in range(hand_point.shape[1]):
        joint_i_mesh = o3d.geometry.TriangleMesh.create_sphere(radius=5.0)
        joint_i_mesh.compute_vertex_normals()
        joint_i_mesh.paint_uniform_color([0.1, 0.7, 0.1])
        joint_transform[:3, 3] = hand_point[:, joint_i] * 1000

        joint_i_mesh.transform(joint_transform)
        joints.append(joint_i_mesh)

    return joints


def load_head():
    # TODO
    pass


def add(objects, vis):
    for obj in objects:
        vis.add_geometry(obj)

    return vis


def remove(objects, vis):
    for obj in objects:
        vis.remove_geometry(obj)

    return vis



if __name__ == "__main__":
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    opt = vis.get_render_option()
    opt.point_size = 4

    dir_seq = './data/yanik/pointClouds_2306/'

    dir_seq = './data/yanik/pointClouds_0507/'

    dir_depth = join(dir_seq, 'depth')
    dir_rgb = join(dir_seq, 'rgb')




    frame_number = 5

    depth, cam_depth_calib, pcd, cam_depth, cam_depth_origin = create_point_cloud_from_depth(dir_depth, frame_number, depth_type='ahat')


    rgb, cam_rgb_calib, cam_rgb, cam_rgb_origin = load_rgb(dir_rgb, frame_number)

    # T_rgb = cam_rgb_calib['M_color']
    # T_depth = cam_depth_calib['M_dist']

    # print('rgb', T_rgb)
    # print('depth', T_depth)

    # T_relative = np.dot(T_depth, np.linalg.inv(T_rgb))    
    # r =  Rotation.from_matrix(T_relative[:3, :3])
    # angles = r.as_euler("zyx",degrees=True)
    # # print('T_relative', T_relative)
    # print('angles', angles)
    # print('translation', T_relative[:3, 3])

    # # pcd = get_colored_pcd(pcd, rgb, cam_rgb_calib)
    # pcd = get_colored_pcd(pcd, rgb, cam_rgb_calib, cam_depth_calib)

    pcd_2 = o3d.io.read_point_cloud(f'{dir_depth}/{frame_number:06d}.ply')
    pcd_2.paint_uniform_color([0.1, 0.1, 0.7])
    points = np.array(pcd_2.points)*1000.
    points[:, 2] *= -1

    pcd_2.points = o3d.utility.Vector3dVector(points)

    pcd_2.transform(cam_depth_calib['M_dist'])

    pcd_2 = get_colored_pcd(pcd_2, rgb, cam_rgb_calib, cam_depth_calib)
    pcd_2, ind = pcd_2.remove_radius_outlier(nb_points=10, radius=10)


    # new_axis_transform = np.eye(4)
    # new_axis_transform[2, 2] = -1
    # new_axis_transform[1, 1] = -1

    # pcd_2.transform(new_axis_transform)

    new_axis_transform = np.eye(4)
    new_axis_transform[0, 0] = -1
    new_axis_transform[2, 2] = -1

    # pcd_2.transform(new_axis_transform)


    # T_new = np.linalg.inv(np.dot(np.linalg.inv(T_rgb), np.linalg.inv(T_depth)))
    # pcd_2.transform(T_new)
    # cam_depth_calib['M_dist'] = T_new
    # pcd_2 = get_colored_pcd(pcd_2, rgb, cam_rgb_calib, cam_depth_calib)


    objects = []

    objects.append(cam_depth_origin)
    objects.append(cam_depth)
    objects.append(cam_rgb)
    objects.append(cam_rgb_origin)
    # objects.append(pcd)
    objects.append(pcd_2)

    world_origin = o3d.geometry.TriangleMesh.create_sphere(radius=10.0)
    world_origin.compute_vertex_normals()
    world_origin.paint_uniform_color([0.1, 0.7, 0.])

    objects.append(world_origin)

    # pcd_2.transform(T_new)
    # objects.append(pcd_2)

    vis = add(objects, vis)
    vis.poll_events()
    vis.update_renderer()

    vis.run()
    # vis = remove(objects, vis)
    cv2.waitKey(1)

    vis.run()




