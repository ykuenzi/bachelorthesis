import numpy as np
import open3d as o3d
import png
import cv2
from scipy.interpolate import NearestNDInterpolator
# import imageio
from segmentation import get_prediction

try:
    from itertools import imap
except ImportError:
    # Python 3...
    imap = map


def get_handpose_connectivity():
    # Hand joint information is in https://github.com/microsoft/psi/tree/master/Sources/MixedReality/HoloLensCapture/HoloLensCaptureExporter
    return [
        [0, 1],

        # Thumb
        [1, 2],
        [2, 3],
        [3, 4],
        [4, 5],

        # Index
        [1, 6],
        [6, 7],
        [7, 8],
        [8, 9],
        [9, 10],

        # Middle
        [1, 11],
        [11, 12],
        [12, 13],
        [13, 14],
        [14, 15],

        # Ring
        [1, 16],
        [16, 17],
        [17, 18],
        [18, 19],
        [19, 20],

        # Pinky
        [1, 21],
        [21, 22],
        [22, 23],
        [23, 24],
        [24, 25]
    ]


def read_hand_pose_txt_new(hand_path, is_stereokit=False):
    #  The format for each entry is: Time, IsGripped, IsPinched, IsTracked, IsActive, {Joint values}, {Joint valid flags}, {Joint tracked flags}
    hand_array = []
    with open(hand_path) as f:
        lines = f.read().split('\n')
        for line in lines:
            if line == '':  # end of the lines.
                break
            hand = []
            if is_stereokit:
                line_data = list(map(float, line.split('\t')))

                if line_data[3] == 0.0:  # if hand pose does not exist.
                    # add empty hand location
                    hand_array.append(line_data[:4]+[0]*3*26)
                elif line_data[3] == 1.0:  # if hand pose does exist.
                    line_data_reshape = np.reshape(
                        line_data[4:], (-1, 4, 4))  # (x,y,z) 3, 7 ,11

                    line_data_xyz = []
                    for line_data_reshape_elem in line_data_reshape:
                        # To get translation of the hand joints
                        location = np.dot(line_data_reshape_elem,
                                        np.array([[0, 0, 0, 1]]).T)
                        line_data_xyz.append(location[:3].T[0])

                    line_data_xyz = np.array(line_data_xyz).T
                    hand = line_data[:4]
                    hand.extend(line_data_xyz.reshape(-1))
                    hand_array.append(hand)
            else:
                line_data = list(map(float, line.split('\t')))
                line_data_reshape = np.reshape(
                    line_data[2:-52], (-1, 4, 4))  # (x,y,z) 3, 7 ,11

                line_data_xyz = []
                for line_data_reshape_elem in line_data_reshape:
                    # To get translation of the hand joints
                    location = np.dot(line_data_reshape_elem,
                                    np.array([[0, 0, 0, 1]]).T)
                    line_data_xyz.append(location[:3].T[0])

                line_data_xyz = np.array(line_data_xyz).T
                hand = line_data[:4]
                hand.extend(line_data_xyz.reshape(-1))
                hand_array.append(hand)
        hand_array = np.array(hand_array)
    return hand_array


def read_hand_pose_txt_old(hand_path):
    hand_array = []
    with open(hand_path) as f:
        lines = f.read().split('\n')
        for line in lines:
            if line == '':  # end of the lines.
                break
            hand = []
            line_data = list(map(float, line.split('\t')))
            if line_data[3] == 0.0:  # if hand pose does not exist.
                # add empty hand location
                hand_array.append(line_data[:4]+[0]*3*26)
            elif line_data[3] == 1.0:  # if hand pose does exist.
                line_data_reshape = np.reshape(
                    line_data[4:], (-1, 4, 4))  # (x,y,z) 3, 7 ,11

                line_data_xyz = []
                for line_data_reshape_elem in line_data_reshape:
                    # To get translation of the hand joints
                    location = np.dot(line_data_reshape_elem,
                                      np.array([[0, 0, 0, 1]]).T)
                    line_data_xyz.append(location[:3].T[0])

                line_data_xyz = np.array(line_data_xyz).T
                hand = line_data[:4]
                hand.extend(line_data_xyz.reshape(-1))
                hand_array.append(hand)
        hand_array = np.array(hand_array)
    return hand_array



def read_hand_pose_txt(hand_path, is_stereokit=False):
    #  The format for each entry is: Time, IsGripped, IsPinched, IsTracked, IsActive, {Joint values}, {Joint valid flags}, {Joint tracked flags}
    hand_array = []
    with open(hand_path) as f:
        lines = f.read().split('\n')
        for line in lines:
            if line == '':  # end of the lines.
                break
            hand = []
            if is_stereokit:
                line_data = list(map(float, line.split('\t')))
                if line_data[3] == 0.0:  # if hand pose does not exist.
                    # add empty hand location
                    hand_array.append(line_data[:4]+[0]*3*26)
                elif line_data[3] == 1.0:  # if hand pose does exist.
                    line_data_reshape = np.reshape(
                        line_data[4:], (-1, 4, 4))  # (x,y,z) 3, 7 ,11

                    line_data_xyz = []
                    for line_data_reshape_elem in line_data_reshape:
                        # To get translation of the hand joints
                        location = np.dot(line_data_reshape_elem,
                                        np.array([[0, 0, 0, 1]]).T)
                        line_data_xyz.append(location[:3].T[0])

                    line_data_xyz = np.array(line_data_xyz).T
                    hand = line_data[:4]
                    hand.extend(line_data_xyz.reshape(-1))
                    hand_array.append(hand)
            else:
                line_data = list(map(float, line.split('\t')))
                line_data_reshape = np.reshape(
                    line_data[2:-52], (-1, 4, 4))  # For version2: line_data[5:-52]

                line_data_xyz = []
                for line_data_reshape_elem in line_data_reshape:
                    # To get translation of the hand joints
                    location = np.dot(line_data_reshape_elem,
                                    np.array([[0, 0, 0, 1]]).T)
                    line_data_xyz.append(location[:3].T[0])

                line_data_xyz = np.array(line_data_xyz).T
                hand = line_data[:4]
                hand.extend(line_data_xyz.reshape(-1))
                hand_array.append(hand)
        hand_array = np.array(hand_array)
    return hand_array


def depthConversion(PointDepth, f, cx, cy):
    H = PointDepth.shape[0]
    W = PointDepth.shape[1]

    columns, rows = np.meshgrid(np.linspace(0, W-1, num=W), np.linspace(0, H-1, num=H))
    distance_from_center = ((rows - cy)**2 + (columns - cx)**2) ** 0.5
    plane_depth = PointDepth / (1 + (distance_from_center / f)**2) ** 0.5

    return plane_depth


axis_transform = np.linalg.inv(
    np.array([[0, 0, 1, 0], [-1, 0, 0, 0], [0, -1, 0, 0], [0, 0, 0, 1]]))


def generatepointcloud(depth, Fx, Fy, Cx, Cy):
    rows, cols = depth.shape
    c, r = np.meshgrid(np.arange(cols), np.arange(rows), sparse=True)
    depth_scale = 1
    z = depth * depth_scale

    x = z * (c - Cx) / Fx
    y = z * (r - Cy) / Fy
    points = np.dstack((x, y, z))
    points = points.reshape(-1, 3)
    points = points[~np.all(points == 0, axis=1)]
    return points


# def load_depth(path):
#     # PyPNG library is used since it allows to save 16-bit PNG
#     r = png.Reader(filename=path)
#     im = np.vstack(imap(np.uint16, r.asDirect()[2])).astype(np.float32)#[:, ::-1]
#     # im = np.vstack(imap(r.asDirect()[2])).astype(np.float32)#[:, ::-1]
#     return im

# Removes plane from the point cloud
def remove_plane(pcd: o3d.geometry.PointCloud) -> o3d.geometry.PointCloud:
    _, inliers = pcd.segment_plane(distance_threshold=.01, ransac_n=3,
                                   num_iterations=1000)
    return pcd.select_by_index(inliers, invert=True)


def load_depth(path):
    # d = imageio.imread(path)
    d = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    d = d[:, :, 1]*1. + d[:, :, 2] * 256.
    d = d.astype(np.float32)
    return d


def save_depth(path, im):
    # PyPNG library is used since it allows to save 16-bit PNG
    w_depth = png.Writer(im.shape[1], im.shape[0], greyscale=True, bitdepth=16)
    im_uint16 = np.round(im).astype(np.uint16)
    with open(path, 'wb') as f:
        w_depth.write(f, np.reshape(im_uint16, (-1, im.shape[1])))


def project_2d(points3d, K, R=np.eye(3), t=np.zeros(3),
               dist_coeffs=np.zeros(5,)):
    print(R, t, K, dist_coeffs)
    pts2d, _ = cv2.projectPoints(points3d, R, t, K, dist_coeffs)
    return pts2d


def project_2d_kinect(points3d, cam_calib_data):
    R = np.array(cam_calib_data['M_color'])[:3, :3]
    t = np.array(cam_calib_data['M_color'])[:3, 3]
    K = np.array(cam_calib_data['K_color'])
    pts2d = project_2d(points3d, K, R, t)
    return pts2d


def get_best_joint(joints: np.ndarray, cam_calib_rgb) -> np.ndarray:
    # Joints good joints to track as they are not likely to be
    # obscured by objects
    indices = [1, 3, 4]
    for idx in indices:
        joint = joints[idx]
        if not joint.any():
            print(f"{idx} not tracked")
            continue
        # Make sure the joint projects onto the image
        joint2d = project_2d_kinect(np.array([joint]), cam_calib_rgb)
        joint2d = np.reshape(joint2d, (-1, 2))
        if joint2d[0, 0] < 0 or joint2d[0, 1] < 0 or \
                joint2d[0, 0] >= 896 or joint2d[0, 1] >= 504:
            print(f"{joint2d} not in range")
            continue

        return joint2d

    return None

# TODO: split left and right hand, segment based only on wrists?
def get_colored_pcd(pcd, rgb, cam_calib_rgb, M_depth, joints):
    MIN_DIST = 600
    MAX_DIST = 2500

    pcd = o3d.cpu.pybind.geometry.PointCloud(pcd)
    pcd.transform(np.linalg.inv(M_depth))
    points = np.array(pcd.points)
    points2d = project_2d_kinect(points, cam_calib_rgb)
    height, width = rgb.shape[:2]

    colors = np.zeros_like(points, dtype='float32')
    indices = []

    # Perform transformation on joints to get actual world coordinates
    # joints = 1000 * np.array([p for p in joints if p.any()])
    joints *= 1000
    joints[:, 2] *= -1
    left_hand_joints = joints[:27]
    right_hand_joints = joints[27:]

    left_palm = left_hand_joints[2]
    right_palm = right_hand_joints[2]

    left_joint2d = get_best_joint(left_hand_joints, cam_calib_rgb)
    # A suitable joint on the left hand is tracked
    if left_joint2d is not None:
        hand_mask = get_prediction(rgb, left_joint2d)

        # Assign color to left hand
        for i in range(points2d.shape[0]):
            dx = int(points2d[i, 0, 0])
            dy = int(points2d[i, 0, 1])

            if dx < width and dx > 0 and dy < height and dy > 0\
                    and hand_mask[dy, dx]:
                colors[i, :] = rgb[dy, dx, ::-1]/255.
                indices.append(i)

    right_joint2d = get_best_joint(right_hand_joints, cam_calib_rgb)
    # A suitable joint on the right hand is tracked
    if right_joint2d is not None:
        hand_mask = get_prediction(rgb, right_joint2d)

        # Assign color to left hand
        for i in range(points2d.shape[0]):
            dx = int(points2d[i, 0, 0])
            dy = int(points2d[i, 0, 1])

            if dx < width and dx > 0 and dy < height and dy > 0\
                    and hand_mask[dy, dx]:
                colors[i, :] = rgb[dy, dx, ::-1]/255.
                indices.append(i)

    # Find the point closest to a joint that is not part of either hand
    obj_points = []
    left_palm_to_thumb = left_hand_joints[6] - left_palm
    right_palm_to_thumb = right_hand_joints[6] - right_palm
    for i in range(np.min(indices) - 1000, np.max(indices) + 1001):
        if i in indices:
            continue
        left_diff = points[i] - left_palm
        right_diff = points[i] - right_palm
        left_dist = left_diff.T @ left_diff
        right_dist = right_diff.T @ right_diff
        if left_dist < right_dist:
            orientation = left_palm_to_thumb.T @ left_diff
            dist = left_dist
        else:
            orientation = right_palm_to_thumb.T @ right_diff
            dist = right_dist

        # Point is too close or far away from the palm, possibly not on object
        # Also exclude points that are on the back of the hand
        if dist <= MIN_DIST or dist >= MAX_DIST or orientation < 0:
            continue

        if np.random.uniform() < \
                .05:
                # .15 * (1 - (dist - MIN_DIST) / (MAX_DIST - MIN_DIST)):
            obj_points.append(points[i])

    obj2d = project_2d_kinect(np.array(obj_points), cam_calib_rgb)
    obj2d = np.reshape(obj2d, (-1, 2))
    print(obj2d)
    obj_mask = get_prediction(rgb, obj2d)

    # TODO: use some threshold to avoid errors if no object is being held
    # Assign color to object
    for i in range(points2d.shape[0]):
        dx = int(points2d[i, 0, 0])
        dy = int(points2d[i, 0, 1])

        if dx < width and dx > 0 and dy < height and dy > 0 \
                and obj_mask[dy, dx]:
            colors[i, :] = rgb[dy, dx, ::-1]/255.
            indices.append(i)

    pcd_colored = o3d.geometry.PointCloud(pcd)
    pcd_colored.colors = o3d.utility.Vector3dVector(colors)

    return pcd_colored.select_by_index(indices)


def map_depth_to_rgb(pcd, rgb, cam_calib_rgb,
                     cam_calib_depth, reference='depth', interpolate=True):
    M_depth = cam_calib_depth['M_dist']
    M_color = np.array(cam_calib_rgb['M_color'])
    pcd_points_depth = np.array(pcd.points)

    pcd_points_depth = np.c_[pcd_points_depth,
                             np.ones(pcd_points_depth.shape[0])]

    pcd_points = np.dot(np.dot(M_color, np.linalg.inv(M_depth)),
                        pcd_points_depth.transpose())
    pcd_points = pcd_points.transpose()[:, :3]
    print(pcd_points[:4, :])

    pcd.transform(np.linalg.inv(M_depth))
    pcd.transform(M_color)

    pcd_points = np.array(pcd.points)
    print(pcd_points[:4, :])
    assert False
    points2d = project_2d(pcd_points, np.array(cam_calib_rgb['K_color']))

    height, width = rgb.shape[:2]

    depth = np.zeros((height, width))

    x = []
    y = []
    z = []
    for i in range(points2d.shape[0]):
        dx = int(points2d[i, 0, 0])
        dy = int(points2d[i, 0, 1])

        if reference == 'depth':
            d_i = pcd_points_depth[i, 2]
        elif reference == 'rgb':
            d_i = pcd_points[i, 2]
        else:
            assert False, 'unknown reference'

        if dx < width and dx > 0 and dy < height and dy > 0:
            if d_i < 0:
                continue
            # Handle 3D occlusions
            if depth[dy, dx] == 0 or d_i < depth[dy, dx]:
                depth[dy, dx] = d_i

                x.append(dx*1./width)
                y.append(dy*1./height)
                z.append(d_i)

    if interpolate:
        X = np.array(range(width))*1./width
        Y = np.array(range(height))*1./height
        X, Y = np.meshgrid(X, Y)  # 2D grid for interpolation
        interp = NearestNDInterpolator(np.array(list(zip(x, y)))
                                       .reshape(-1, 2), z)
        Z = interp(X, Y)

        mask = np.zeros_like(depth).astype('uint8')
        indices = np.where(depth > 0)
        mask[indices] = 255
        mask = cv2.blur(mask, (5, 5))
        indices = np.where(mask > 0)
        depth[indices] = Z[indices]

        depth = cv2.medianBlur(depth.astype('float32'), 5)

    return depth
